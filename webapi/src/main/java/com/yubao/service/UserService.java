package com.yubao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import com.yubao.entity.Question;
import com.yubao.mapper.QuestionMapper;
import com.yubao.request.AddUserReq;
import com.yubao.request.LoginRequest;
import com.yubao.request.UpUserReq;
import com.yubao.util.MD5;
import com.yubao.response.PageObject;
import com.yubao.response.UserViewModel;
import com.yubao.mapper.UserMapper;
import com.yubao.entity.User;
import com.yubao.entity.UserExample;
import com.yubao.util.NormalException;
import com.yubao.util.UuidUtil;
import org.apache.ibatis.annotations.Param;
import org.assertj.core.util.Strings;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by Administrator on 2016-11-30.
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> implements IService<User> {
	@Autowired
    UserMapper userMapper;

	@Autowired
    CacheService cacheService;

	@Autowired
    LoginService loginService;

    DozerBeanMapper mapper = new DozerBeanMapper();

    public  String login(LoginRequest req) throws NormalException, UnsupportedEncodingException, NoSuchAlgorithmException {
        QueryWrapper<User> ew = new QueryWrapper<User>();
        ew.eq("Account", req.getAccount());

        User userInfo = getOne(ew);
        if (userInfo == null) {
            throw new NormalException("登录用户不存在");
        }

        if (!userInfo.getPwd().equals(MD5.Encode(req.getPwd()))) {
            throw new NormalException("登录密码错误");
        }

        UserViewModel loginUser = mapper.map(userInfo, UserViewModel.class);
        String token = UuidUtil.getUUID();
        cacheService.create(token, loginUser);
        return token;
    }

    public PageObject<UserViewModel> GetNew() {
        int index = 1;
        int size = 12;

        UserExample exp = new UserExample();

        exp.setOrderByClause("createtime desc");


        PageObject<UserViewModel> obj = new PageObject<UserViewModel>();
        obj.setSize(size);

        obj.setTotal(userMapper.countByExample(exp));

        int startindex = (index-1)*size;
        exp.setOffset(startindex);
        exp.setLimit(size);
        List<User> users= userMapper.selectByExample(exp);
        if(users != null && users.size() > 0){
            obj.setObjects(UserViewModel.From(users));
        }

        return obj;
    }

    public String regNew(AddUserReq req) throws NormalException, UnsupportedEncodingException, NoSuchAlgorithmException {

        String[] invalidAccounts = new String[]{"admin","administrator","管理员","超级管理员"};
        if(Arrays.asList(invalidAccounts).contains(req.getAccount()) || Arrays.asList(invalidAccounts).contains(req.getName())){
            throw new NormalException("非法账号或用户名字");
        }

        if(Strings.isNullOrEmpty(req.getAccount())){
            throw new NormalException("账号不能为空");
        }

        if(count(new QueryWrapper<User>().eq("Account", req.getAccount())) > 0){
            throw new NormalException("账号已存在");
        }

        User user = mapper.map(req, User.class);
        user.setPwd(MD5.Encode(req.getPwd()));
        user.setId(UUID.randomUUID().toString());
        user.setPic(new Random().nextInt(9) +".jpg");
        user.setCreatetime(new Date());
        save(user);
        return user.getId();
    }

    public void up(UpUserReq req) throws NormalException {
        UserViewModel loginuser = loginService.get();
        if (loginuser == null)
            throw new NormalException("亲！等个录先~~");

        if(loginuser.getAuth() != 1){
            throw new NormalException("只有管理员才能进行该操作");
        }

        User user = getById(req.getUid());
        if(user == null){
            throw new NormalException("该用户id不存在");
        }
        user.setRmb(req.getRmb());
        saveOrUpdate(user);
    }
}
